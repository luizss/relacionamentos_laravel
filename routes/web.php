<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//relacionamentos one-to-one
//Rota para recuperar os dados através do nome de um país
$this->get('one-to-one', 'OneToOneController@OneToOne');
//Rota para recuperar os dados de um país através de suas latitudes e longitudes
$this->get('one-to-one-inverse', 'OneToOneController@OneToOneInverse');
//Rota para a função de inserir os dados
$this->get('one-to-one-insert', 'OneToOneController@OneToOneInsert');

//relacionamentos one-to-many
$this->get('one-to-many','OneToManyController@oneToMany');
$this->get('one-to-many-two','OneToManyController@oneToManyTwo');
$this->get('one-to-many-insert','OneToManyController@oneToManyInsert');
$this->get('one-to-many-insert-two','OneToManyController@oneToManyInsertTwo');
$this->get('many-to-one','OneToManyController@manyToOne');

//has many through

$this->get('has-many-through','OneToManyController@hasManyThrough');

/**
 * many to many
 */
$this->get('many-to-many','ManyToManyController@manyToMany');
$this->get('many-to-many-inverse','ManyToManyController@manyToManyInverse');
$this->get('many-to-many-insert','ManyToManyController@manyToManyInsert');

/*
 * Relacionamentos polimórficos
 */
$this->get('polymorphics','PolymorphicController@polymorphic');
$this->get('polymorphic-insert','PolymorphicController@polymorphicInsert');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/country/novo', 'CountriesController@novo');
Route::post('/country/salvar', 'CountriesController@salvar');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
