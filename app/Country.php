<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'name'
    ];

    public function location()
    {
        //A linha abaixo faz a ligação entre o model location e o model country
        return $this->hasOne(Location::class, 'country_id');
    }

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function cities(){
        return $this->hasManyThrough(City::class,State::class);
    }
    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }
}
