<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'name',
        'initials',
        'country_id'
    ];

    public function country()
    {
        //Forma mais simples, seguindo o padrão laravel
        return $this->belongsTo(Country::class);
        //forma com parametros, onde se especifica que country_id vincula as duas models por meio
        //do campo id, da tabela countries
        //return $this->belongsTo(Country::class,'country_id','id');

    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }
}
