<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['latitude','longitude'];
    public function country()
    {
        //A linha abaixo faz a ligação entre o model country e o model location
        return $this->belongsTo(Country::class);
    }
}
