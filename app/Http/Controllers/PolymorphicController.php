<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\State;
use App\Country;
use App\Comment;

class PolymorphicController extends Controller
{
    public function polymorphic()
    {
        /*
         * comentários por cidade
        $city = City::where('name','Manaus')->get()->first();
        echo "<b>{$city->name}</b><br>";

        $comments = $city->comments()->get();

        foreach($comments as $comment){
            echo "<br>{$comment->description}<hr>";
        }
         */
        /*
         * comentários por estado
          $state = State::where('name','Amazonas')->get()->first();
        echo "<b>{$state->name}</b><br>";

        $comments = $state->comments()->get();

        foreach($comments as $comment){
            echo "<br>{$comment->description}<hr>";
        }
         */
        /*
         * Comentários por páis
         */
        $country = Country::where('name','Brasil')->get()->first();
        echo "<b>{$country->name}</b><br>";

        $comments = $country->comments()->get();

        foreach($comments as $comment){
            echo "<br>{$comment->description}<hr>";
        }
    }

    public function polymorphicInsert()
    {
        /*
         * cidade
            $city = City::where('name','Manaus')->get()->first();
            echo $city->name;
            $comment = $city->comments()->create(['description'=>"New comment {$city->name}".date('YmdHis'),]);
            var_dump($comment->description);
        */
        /*
         * estado
        $state = State::where('name','Brasil')->get()->first();
        echo $state->name;
        $comment = $state->comments()->create(['description'=>"New comment {$state->name}".date('YmdHis'),]);
        var_dump($comment->description);
         */
        $country = Country::where('name','Brasil')->get()->first();
        echo $country->name;
        $comment = $country->comments()->create(['description'=>"New comment {$country->name}".date('YmdHis'),]);
        var_dump($comment->description);

    }
}
