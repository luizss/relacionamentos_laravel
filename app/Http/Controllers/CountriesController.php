<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers;

class CountriesController extends Controller
{
    public function index(){

    }
    public function salvar(Request $request){
        $country = new Country();
        $country->create($request->all());
        Session::flash('mensagem','País cadastrado com sucesso');
    }
    public function novo(){
        return view('country.formulario');
    }

}
