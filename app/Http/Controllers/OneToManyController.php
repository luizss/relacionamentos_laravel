<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\State;

class OneToManyController extends Controller
{

    public function oneToMany()
    {
        //$country = Country::where('name','Brasil')->get()->first();
        //exemplo de uma pesquisa de usuario
        $keySearch = 'a';

        //$countries = Country::where('name', 'LIKE', "%{$keySearch}%")->get();
        //fazendo uma consulta mais eficiente
        $countries = Country::where('name', 'LIKE', "%{$keySearch}%")->with('states')->get();

        foreach ($countries as $country) {
            echo "<b>{$country->name}</b>";
            //chamando em formato de atributo
            //$states = $country->states;
            //chamando em formato de metodo
            //$states = $country->states()->get();
            //utilizando a forma mais eficiente do um para muitos (one-to-many)
            $states = $country->states;
            //chamando em formato de metodo e utilizando um metodo where
            //$states = $country->states()->where('initials','AM')->get();

            foreach ($states as $state) {
                echo "<br>{$state->initials} - {$state->name}";
            }
            echo "<hr>";
        }
    }

    public function manyToOne()
    {
        $stateName = 'Amazonas';
        $state = State::where('name', $stateName)->get()->first();
        echo "<b>{$state->name}</b>";

        $country = $state->country;
        echo "<br>País: {$country->name}";
    }

    public function oneToManyTwo()
    {
        //$country = Country::where('name','Brasil')->get()->first();
        //exemplo de uma pesquisa de usuario
        $keySearch = 'a';

        //$countries = Country::where('name', 'LIKE', "%{$keySearch}%")->get();
        //fazendo uma consulta mais eficiente
        $countries = Country::where('name', 'LIKE', "%{$keySearch}%")->with('states')->orderBy('name')->get();

        foreach ($countries as $country) {
            //recuperando o nome do país
            echo "<h4>{$country->name}</h4>";
            //chamando em formato de atributo
            //$states = $country->states;
            //chamando em formato de metodo
            //$states = $country->states()->get();
            //utilizando a forma mais eficiente do um para muitos (one-to-many) - recuperando os estados
            $states = $country->states;
            //chamando em formato de metodo e utilizando um metodo where
            //$states = $country->states()->where('initials','AM')->get();

            //Mostrando as informações dos estados - nome
            foreach ($states as $state) {
                echo "<br><b>{$state->initials} - {$state->name}: </b><br>";

                //mostrando recuperando e mostrando as cidades de cada estado
                foreach ($state->cities as $city) {
                    echo "{$city->name}<br> ";
                }
            }
            echo "<hr>";
        }
    }

    public function oneToManyInsert()
    {
        //Método 1
        //recuperando o país pelo id;
        $dataForm = [
            'name' => 'Bahia',
            'initials' => 'BA'
        ];

        $country = Country::find(1);

        $insertState = $country->states()->create($dataForm);

        var_dump($insertState->name);
    }

    public function oneToManyInsertTwo()
    {
        //Método 2
        $dataForm = [
            'name' => 'Bahia',
            'initials' => 'BA',
            'country_id' => '1'
        ];

        $insertState = State::create($dataForm);

        var_dump($insertState->name);
    }

    public function hasManyThrough()
    {
        $country = Country::find(1);
        echo "<b>$country->name</b>";

        $cities = $country->cities;

        foreach ($cities as $city){
            echo "<br>{$city->name}";
        }
        echo "<br>Total de cidades: {$cities->count()}";
    }
}
