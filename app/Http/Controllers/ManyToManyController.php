<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Company;

class ManyToManyController extends Controller
{
    public function manyToMany()
    {
        $city = City::where('name', 'Novo Airão')->get()->first();
        echo "<b>{$city->name}</b><br>";

        $companies = $city->companies;
        foreach ($companies as $company) {
            echo "{$company->name}";
        }

    }

    public function manyToManyInverse()
    {
        $company = Company::where('name','Ralp Corporation')->get()->first();

        echo "<b>{$company->name}</b><br>";

        $cities = $company->cities;

        foreach($cities as $city){
            echo "{$city->name}<br>";
        }
    }

    public function manyToManyInsert(){
        $dataForm = [3,4];
        $company = Company::find(1);
        echo "<b>$company->name:</b><br>";
        //A linha abaixo salva os registros no banco de dados. Ele permite salvar os mesmos arquivos várias vezes
        //$company->cities()attach($dataForm);
        //A linha abaixo salva apenas os dados do array naquele momento. Se a primeira vez se salva 3 dados,
        //na próxima vez que for salvar os mesmos dados, excluindo um deles, deixando apenas dois, ele salva os dois
        //e apaga do banco o dado que não achar no array. Ele sincroniza os dados do array com os dados na tabela do banco de dados
        $company->cities()->sync($dataForm);
        //A linha abaixo apaga a cidade de id igual a 4
        //$company->cities()->detach(4);

        $cities = $company->cities;

        foreach($cities as $city){
            echo "{$city->name}<br>";
        }

    }
}
