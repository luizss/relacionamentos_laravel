<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Location;

class OneToOneController extends Controller
{
    public function OneToOne()
    {
        $country = Country::find(1);
        //$country = Country::where('name','Brasil')->get()->first();

        echo $country->name;

        //$location = $country->location;
        $location = $country->location()->get()->first();

        echo "<hr>Latitude: {$location->latitude}<br>";
        echo "Longitude: {$location->longitude}";
    }

    public function OneToOneInverse()
    {
        $latitude = 123;
        $longitude = 321;

        //A linha abaixo recupera a quantidade de registros que possuem latitude e longitude iguais aos valores das variaveis
        //$location = Location::where('latitude',$latitude)->where('longitude',$longitude)->get()->count();
        //echo "Quantidade de registros: $location";

        $location = Location::where('latitude', $latitude)->where('longitude', $longitude)->get()->first();

        $country = $location->country;
        echo "País: $country->name";
    }

    public function OneToOneInsert()
    {
        $dataForm = [
            'name' => 'França',
            'latitude' => '43',
            'longitude' => '34'
        ];
        //cadastrando no banco de dados o país
        $country = Country::create($dataForm);
        /*
        //cadastrando latitude e longitude
        $location = new Location();
        $location->latitude = $dataForm['latitude'];
        $location->longitude = $dataForm['longitude'];
        //recuperando o id do país
        $location->country_id = $country->id;

        //cadastrando os dados
        $saveLocation = $location->save();
        */
        $location = $country->location()->create($dataForm);
        var_dump($location);
    }
}
