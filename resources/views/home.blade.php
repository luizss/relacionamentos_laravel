@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-xs-12">
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li><a class="btn btn-sm btn-default" href="{{ url('country/novo') }}">Cadastrar país</a></li>&nbsp;
                    <li><a class="btn btn-sm btn-default" href=''>Cadastrar localização</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection
